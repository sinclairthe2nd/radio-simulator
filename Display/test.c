#include <stdio.h>
#include <string.h>
#include <wiringPi.h>

int main (int argc, char* argv[]) {
	wiringPiSetup () ;	
	pinMode(30,OUTPUT);
	pinMode(10,OUTPUT);
	pinMode(11,OUTPUT);
	pinMode(6,OUTPUT);
	int a;
   	char str1[17];
   	char str2[2];
   	int result;

   	//Assigning the value to the string str1
   	strcpy(str1, argv[1]);

   	//Assigning the value to the string str2
   	strcpy(str2, "0");


	for(a = 0; a < 16; a++){
   	//This will compare the first character
   	result = strncmp(str1, str2, 1);
	
   	if(result > 0)      {
      		printf("Is One\n");
		digitalWrite(11,1);
		            }
	else if(result == 0) {
                printf("Is Zero\n");
		digitalWrite(11,0);
                            } 
	digitalWrite(6,1);
	delay(1);
	digitalWrite(6,0);

	memmove(str1, str1+1, strlen(str1));	
	printf("String1: %s\n",str1);
	}
	digitalWrite(10,1);
	delay(1);
	digitalWrite(10,0);
   return 0;
}
